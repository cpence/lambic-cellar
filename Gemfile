source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.3.0"

gem "rails", "~> 7.1.3"
gem "sprockets-rails"

gem "pg", "~> 1.1"
gem "puma", "~> 6.0"

gem "omniauth"
gem "omniauth-google-oauth2"
gem "omniauth-rails_csrf_protection"

gem "haml"
gem "importmap-rails"
gem "kramdown"

group :production do
  gem "rack-timeout"
end

group :development, :test do
  gem "debug"

  gem "factory_bot_rails"
end

group :development do
  gem "web-console"

  gem "syntax_tree"
  gem "prettier_print"
  gem "syntax_tree-haml"
  gem "syntax_tree-rbs"

  gem "yard"
  gem "yardstick"

  gem "brakeman"
  gem "bundle-audit"
  gem "rails_best_practices"
  gem "reek"
  gem "rubocop"
  gem "rubocop-rails"
end

group :test do
  gem "shoulda"

  gem "capybara"
  gem "selenium-webdriver", "~> 4.11"
end
