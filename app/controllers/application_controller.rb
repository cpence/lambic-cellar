# Base class for all controllers
class ApplicationController < ActionController::Base
  # Exception raised when an unauthorized action is accessed
  NotAuthorized = Class.new(StandardError)

  rescue_from ApplicationController::NotAuthorized do
    render_error_page(
      status: :forbidden,
      header: "Not authorized!",
      explanation: <<~EXP
        You need to be logged in to visit the page that you tried to visit. Try
        returning to the front page, logging in again (or logging out and back
        in), and visiting the page you were trying to visit again.
      EXP
    )
  end

  # Render our basic error template
  #
  # This method renders a user-friendly error page, providing details about the
  # error.
  #
  # @param status [Integer] the HTTP status to return
  # @param header [String] the header to show at the top of the error page
  # @param explanation [String] the explanation of the error, which will be
  #   parsed as Markdown
  # @return [void]
  def render_error_page(status:, header:, explanation:)
    render template: "layouts/error",
           status:,
           layout: false,
           locals: {
             header:,
             explanation: Kramdown::Document.new(explanation).to_html.html_safe
           }
  end

  # Ensure that the user is authenticated
  #
  # This method is designed to be called as a `before_action` to ensure that the
  # user is authenticated before they visit an action.
  #
  # @raise [NotAuthorized] if the user is not logged in
  # @return [void]
  def authenticate_user!
    raise NotAuthorized unless user_signed_in?
  end

  protect_from_forgery with: :exception

  helper_method :current_user
  helper_method :user_signed_in?

  before_action :redirect_subdomain

  private

  # Redirect the WWW subdomain back to the root
  #
  # @api private
  # @return [void]
  def redirect_subdomain
    return unless request.host == "www.lambic-cellar.be"

    redirect_to "https://lambic-cellar.be#{request.fullpath}",
                status: :moved_permanently
  end

  # Returns the currently logged-in user, or `nil`
  #
  # @api private
  # @return [User] the current user
  def current_user
    @current_user ||=
      (User.find_by(id: session[:current_user_id]) if session[:current_user_id])
  end

  # Returns true if there is a user currently signed in
  #
  # @api private
  # @return [Boolean] true if a user is signed in
  def user_signed_in?
    current_user.present?
  end
end
