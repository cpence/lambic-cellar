class BeersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_beer, only: %i[show edit update destroy]

  def index
    @beers = Beer.all
  end

  def show
  end

  def new
    @beer = Beer.new
  end

  def edit
  end

  def create
    @beer = Beer.new(beer_params)

    if @beer.save
      redirect_to beer_url(@beer), notice: I18n.t(".success")
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @beer.update(beer_params)
      redirect_to beer_url(@beer), notice: I18n.t(".success")
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @beer.destroy!
    redirect_to beer_url, notice: I18n.t(".success")
  end

  private

  def set_beer
    @beer = Beer.find(params[:id])
  end

  def beer_params
    params.require(:beer).permit(
      :name,
      :brewery_id,
      :disambiguation,
      :description
    )
  end
end
