class BottlesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_bottle, only: %i[show edit update destroy]

  def index
    @bottles = current_user.bottles.all
  end

  def show
  end

  def new
    @bottle = current_user.bottles.new
  end

  def edit
  end

  def create
    @bottle = current_user.bottles.new(bottle_params)

    if @bottle.save
      redirect_to bottle_url(@bottle), notice: I18n.t(".success")
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @bottle.update(bottle_params)
      redirect_to bottle_url(@bottle), notice: I18n.t(".success")
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @bottle.destroy!
    redirect_to bottles_url, notice: I18n.t(".success")
  end

  def drink
  end

  private

  def set_bottle
    @bottle = Bottle.find(params[:id])
  end

  def bottle_params
    params.require(:bottle).permit(
      :name,
      :rule_id,
      :beer_id,
      :bottled_on,
      :description,
      :drunk,
      :drunk_on
    )
  end
end
