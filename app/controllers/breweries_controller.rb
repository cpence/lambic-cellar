class BreweriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_brewery, only: %i[show edit update destroy]

  def index
    @breweries = Brewery.all
  end

  def show
  end

  def new
    @brewery = Brewery.new
  end

  def edit
  end

  def create
    @brewery = Brewery.new(beer_params)

    if @brewery.save
      redirect_to brewery_url(@brewery), notice: I18n.t(".success")
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @brewery.update(brewery_params)
      redirect_to brewery_url(@brewery), notice: I18n.t(".success")
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @brewery.destroy!
    redirect_to brewery_url, notice: I18n.t(".success")
  end

  private

  def set_brewery
    @brewery = Brewery.find(params[:id])
  end

  def brewery_params
    params.require(:brewery).permit(:name)
  end
end
