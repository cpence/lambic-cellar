# Controller responsible for rendering static error routes
class ErrorsController < ApplicationController
  # Render a 404 error page
  #
  # @return [void]
  def not_found
    render_error_page(
      status: :not_found,
      header: "Page not found!",
      explanation: <<~EXP
        The page that you've requested could not be found. Try going back to
        [the main page](/) and navigating from there. If you're still getting
        this error, [you can always report a bug
        here.](https://codeberg.org/cpence/lambic-cellar/issues)
      EXP
    )
  end

  # Render a 422 error page
  #
  # @return [void]
  def unprocessable_entity
    render_error_page(
      status: :unprocessable_entity,
      header: "The change you wanted was rejected",
      explanation: <<~EXP
        Maybe you tried to change something that you didn't have access to? Try
        going back to [the main page](/) and navigating from there. If you're
        still getting this error, [you can always report a bug
        here.](https://codeberg.org/cpence/lambic-cellar/issues)
      EXP
    )
  end

  # Render a 500 error page
  #
  # @return [void]
  def internal_server_error
    render_error_page(
      status: :internal_server_error,
      header: "Server error!",
      explanation: <<~EXP
        We're sorry, but something went wrong with our servers or code. Try
        going back to [the main page](/) and navigating from there. If you're
        still getting this error, [you can always report a bug
        here.](https://codeberg.org/cpence/lambic-cellar/issues)
      EXP
    )
  end
end
