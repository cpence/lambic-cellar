# Controller that handles user and session objects
class SessionsController < ApplicationController
  before_action :authenticate_user!, only: %i[show delete]

  # Log in a user
  #
  # This is called as a callback from the OmniAuth OpenID login process. We look
  # up the user record (or create a new one) using the response that we've
  # received from Google.
  #
  # @return [void]
  def create
    @current_user = User.find_or_create_from_google(google_response)
    session["current_user_id"] = @current_user.id

    # See whether this user is new or not
    flash =
      if @current_user.new_user
        I18n.t("sessions.new_user")
      else
        I18n.t("sessions.signed_in")
      end

    redirect_to root_url, notice: flash
  end

  # Log out the user
  #
  # All we need to do here is reset the session and redirect. Note that we
  # don't care if you're logged in when you call this method; it will reset the
  # session anyway.
  #
  # @return [void]
  def destroy
    reset_session
    redirect_to root_url, notice: I18n.t("sessions.signed_out")
  end

  # Display a failure page if something goes wrong with user authentication
  #
  # This probably can't be debugged by the user, but we should at least show the
  # error code so that they can use it in a bug report.
  #
  # @return [void]
  def failure
    render_error_page(
      status: :unprocessable_entity,
      header: "Authentication error!",
      explanation: <<~EXP
        We've had some kind of problem communicating between our servers and the
        servers over at Google to create or authenticate your user account. Wait
        a moment and try again, and if things are still broken, [go report a bug
        here.](https://codeberg.org/cpence/lambic-cellar/issues] If you report a
        bug, be sure to indicate that this is an authentication error, and that
        your error code is `#{params[:message]}`.
      EXP
    )
  end

  # Delete the user from the database and destroy all of their data
  #
  # Note that this doesn't do any confirmation; it assumes that the action has
  # already been confirmed client-side.
  #
  # @return [void]
  def delete
    # Delete the user and wipe the session
    current_user.destroy
    reset_session

    redirect_to root_url, alert: I18n.t("sessions.deleted")
  end

  # Show the page for editing user settings
  #
  # @return [void]
  def show
  end

  private

  # Extract the Google response hash from OmniAuth
  #
  # @api private
  # @raise [ActionController::ParameterMissing] if the Google response is
  #   missing or malformed
  # @return [Hash] the Google OpenID response
  def google_response
    ret = request&.env&.dig("omniauth.auth")
    unless ret
      failure
      return
    end

    ret
  end
end
