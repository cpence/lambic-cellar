# Controller for rendering static content pages
class StaticController < ApplicationController
  # Render the root page for users not signed in
  #
  # @return [void]
  def index
  end

  # Render the about page describing how the app works
  #
  # @return [void]
  def about
  end

  # Render the privacy policy
  #
  # @return [void]
  def privacy
  end
end
