# Application-wide helper functions
module ApplicationHelper
  # Patch Markdown support into translations
  #
  # Add support to the global translation helper for translation keys with a
  # `_md` prefix, which will be rendered as Markdown. (This is similar to the
  # translate function's own support for the `_html` prefix.)
  #
  # @param key [String] the locale key to look up
  # @param options [Hash] options, as for `I18n.translate`
  # @return [String] Translated string, possibly parsed as Markdown
  def translate(key, **options)
    res = super(key, **options)
    return res unless key.end_with?("_md")

    # Remove the outer-most paragraph tag here, since we're expecting a
    # span-level kind of content
    html = Kramdown::Document.new(res).to_html
    html = html.delete_prefix("<p>").delete_suffix("\n").delete_suffix("</p>")

    html.html_safe
  end

  # Patch short alias as well
  #
  # @see ApplicationHelper#translate
  alias t translate

  FLASH_MAP = {
    alert: "alert-danger",
    notice: "alert-info"
  }.with_indifferent_access.freeze

  # A map from Rails hash names to Bootstrap alert classes
  #
  # @return [Hash] a mapping from Rails flashes to CSS class names
  def flash_map
    FLASH_MAP
  end

  # Produce a tag for our embedded SVG icons from Bootstrap
  #
  # These icons are placed directly in every page as SVG; see
  # `views/layouts/_svg.html`.
  #
  # @param id [String] the ID of the SVG, without 'svg-'
  # @return the `<svg>` tag for the icon
  def svg_tag(id)
    content_tag :svg,
                class: "bi me-1",
                viewBox: "0 0 16 16",
                role: "img",
                aria: {
                  hidden: "true"
                } do
      tag.use "xlink:href" => "#svg-#{id}"
    end
  end

  # Return the class to add to form controls if there is an error for the
  # given object and attribute.
  #
  # @param object [ApplicationRecord] the model object to check
  # @param attribute [Symbol] the attribute to check
  # @return [String] the class value to add, or blank
  def validate_class(object, attribute)
    if object.errors.empty?
      return ""
    elsif object.errors.include?(attribute)
      return "is-invalid"
    else
      return "is-valid"
    end
  end

  # Return the validation feedback tag if there is an error for the given object
  # and attribute.
  #
  # @param object [ApplicationRecord] the model object to check
  # @param attribute [Symbol] the attribute to check
  # @return the `<div>` tag with the validation feedback, if needed
  def validate_error(object, attribute)
    if object.errors.include?(attribute)
      return(
        content_tag(:div, class: "invalid-feedback") do
          object.errors.full_messages_for(attribute).join("; ")
        end
      )
    end
  end
end
