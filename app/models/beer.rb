# Beer is the representation of a single product from a brewery, which could (at
# least in theory) be brewed multiple times.
#
# @!attribute name
#   The name of this beer
#
#   @raise [ValidationError] if `name` is absent (`validates :presence`)
#   @return [String] the beer's name
# @!attribute brewery
#   The brewery that brewed this beer
#
#   Note that we don't have support for collaboration brews (sorry, this isn't
#   Untappd). You'll have to pick a "primary" brewery.
#
#   @return [Brewery] the brewery that brewed this beer
# @!attribute disambiguation
#   An optional string to distinguish similar beers
#
#   Inspired by MusicBrainz, if this is set it will be shown in the UI in small,
#   grey font whenever the beer is shown to distinguish multiple beers with very
#   similar names.
#
#   @return [String] the beer's disambiguation string
# @!attribute description
#   An optional description of this beer
#
#   @return [String] the beer's description
class Beer < ApplicationRecord
  belongs_to :brewery

  validates :name, presence: true
  validates :beer, presence: true

  # Update the drinking windows for all bottles of this beer.
  #
  # Note that this will _erase_ the drinking windows for any bottles that have
  # them already set, recomputing all of them according to the drinking-window
  # algorithm.
  #
  # The algorithm evenly divides the extant bottles of this beer over the entire
  # aging period of each bottle.
  #
  # @param [User] user the user for whom to update drinking windows
  # @return [void]
  def update_windows(user)
    # Get the list of the user's bottles of this beer
    bottles = user.bottles.where(beer: self, drunk: false)
    return if bottles.empty?

    # Set the window of every bottle to the largest possible
    bottles.each(&:reset_window)

    # Get the lowest window_start date to start off the algorithm
    date = bottles.map(&:window_start).min
    active = active_bottles(bottles, date)

    # Okay, we're off, start the actual algorithm
    until active.empty?
      # Get the new list
      next_date = next_active_change(bottles, date)
      next_active = active_bottles(bottles, next_date)

      # Divide the time period between this date and the next date into an equal
      # number of parts
      time_slice = (next_date - date) / active.count

      # Assign the active bottles to subdivisions, leaving the start of the
      # first bottle and the end of the last bottle as they were
      active.each_with_index do |b, i|
        b.window_start = date if i != 0

        date += time_slice
        b.window_end = date if i != active.count - 1
        b.save
      end

      active = next_active
    end

    fix_overlap(bottles)
  end

  private

  # Get the bottles that are "active" at the given date
  #
  # From the given array of bottles, return those whose window includes the
  # given date. Return them sorted, first by their window_start, and then by
  # their botttled-on date.
  #
  # @param bottles [Array<Bottle>] the list of bottles to check
  # @param date [Date] the date for which to calculate
  # @return [Array<Bottle>] the bottles active at that date
  def active_bottles(bottles, date)
    bottles
      .select { |b| b.window_start <= date && b.window_end > date }
      .sort_by { |b| [b.window_start, b.bottled_on] }
  end

  # The next time the "active" list changes after the given date
  #
  # The next change in the active bottles list happens either when a bottle
  # leaves the list because it's out of the window, or when a new bottle enters
  # the list because we hit the start of its window. Find that date.
  #
  # @param bottles [Array<Bottle>] the list of all bottles to check
  # @param date [Date] the date after which to find the next change
  # @return [Date] the date of the next change in the active list
  def next_active_change(bottles, date)
    ret = Date.new(3000, 1, 1)

    bottles.each do |b|
      ret = b.window_start if b.window_start > date && b.window_start < ret
      ret = b.window_end if b.window_end > date && b.window_end < ret
    end

    ret
  end

  # Fix windows that overlap by one day
  #
  # This code creates drinking windows that overlap by a single day (where the
  # last day of one window is the first day of the following window). Fix that
  # here by adjusting `window_end`.
  #
  # @return [void]
  def fix_overlap(bottles)
    bottles.each_with_index do |b, i|
      if i == bottles.count - 1 || b.window_end == bottles[i + 1].window_start
        b.window_end -= 1.day
        b.save
      end
    end
  end
end
