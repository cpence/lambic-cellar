# Bottle is the representation of a single bottle of a beer, owned by a given
# user.
#
# @!attribute name
#   An optional user-friendly name for this bottle
#
#   If not specified, we just identify the bottle by its bottling date.
#
#   @return [String] the bottle's name
# @!attribute beer
#   The beer that this bottle contains
#
#   @return [Beer] the beer that this bottle contains
# @!attribute rule
#   The aging rule for this bottle
#
#   @return [Rule] the rule for aging this bottle
# @!attribute user
#   The user who owns this bottle
#
#   This is fetched via the owner of the aging rule, and is guaranteed to always
#   be present.
#
#   @return [User] the user who owns this bottle
# @!attribute bottled_on
#   The date on which this beer was bottled
#
#   @raise [ValidationError] if `bottled_on` is absent (`validates :presence`)
#   @return [Date] the bottling date
# @!attribute window_start
#   The start of the drinking window for this bottle
#
#   @return [Date] the start of the drinking window
# @!attribute window_end
#   The end of the drinking window for this bottle
#
#   @return [Date] the end of the drinking window
# @!attribute drunk
#   If true, this bottle has been drunk
#
#   @raise [ValidationError] if `drunk` is absent (`validates :presence`)
#   @return [Boolean] the drunk status
# @!attribute drunk_on
#   The date on which this bottle was drunk
#
#   @return [Date] the drinking date
# @!attribute description
#   An optional description of this bottle
#
#   @return [String] the description of the bottle
class Bottle < ApplicationRecord
  belongs_to :beer
  belongs_to :rule
  has_one :user, through: :rule

  validates :rule, presence: true
  validates :beer, presence: true
  validates :bottled_on, presence: true

  # The earliest date on which this beer should be drunk
  #
  # This returns the `bottled_on` date plus the minimum number of months as
  # specified in the aging rule. If `past` is true, then this date can be in the
  # past, otherwise it will be clamped to "today" if it would otherwise be in
  # the past.
  #
  # @param past [Boolean] if true, allow dates in the past
  # @return [Date] earliest possible drinking date
  def window_earliest(past: false)
    date = bottled_on + rule.min_months.months
    date = Time.zone.today if !past && date < Time.zone.today

    date
  end

  # The latest date on which this beer should be drunk
  #
  # This returns the `bottled_on` date plus the maximum number of months as
  # specified in the aging rule.
  #
  # @return [Date] latest possible drinking date
  def window_latest
    bottled_on + rule.max_months.months
  end

  # Reset the drinking window for this beer to its largest possible value
  #
  # @return [void]
  def reset_window
    self.window_start = window_earliest
    self.window_end = window_latest
    save
  end
end
