# Brewery is the record for a brewery, to which many beers belong.
#
# @!attribute name
#   The brewery's name
#
#   @raise [ValidationError] if `name` is absent (`validates :presence`)
#   @return [String] the brewery's name
# @!attribute beers
#   A list of beers belonging to this brewery
#
#   @return [ActiveRecord::Relation] this brewery's beers
class Brewery < ApplicationRecord
  has_many :beers, dependent: :nullify

  validates :name, presence: true
end
