# Rule is the representation of an aging rule, with minimum and maximum aging
# time.
#
# @!attribute name
#   The user-friendly name of this aging rule
#
#   @raise [ValidationError] if `name` is absent (`validates :presence`)
#   @return [String] the name of this rule
# @!attribute user
#   The user to whom this rule belongs
#
#   This attribute is not validated, but it is guaranteed by a non-null foreign
#   key constraint, so it will always be present.
#
#   @return [User] the user who owns this rule
# @!attribute bottles
#   The bottles that are subject to this rule
#
#   @return [ActiveRecord::Relation] this rule's bottles
# @!attribute min_months
#   The minimum number of months for aging this beer before drinking
#
#   @raise [ValidationError] if `min_months` is absent (`validates :presence`)
#   @return [Integer] the minimum number of months to age
# @!attribute max_months
#   The maximum number of months of age before drinking this beer
#
#   @raise [ValidationError] if `max_months` is absent (`validates :presence`)
#   @return [Integer] the maximum number of months to age
# @!attribute description
#   An optional description of this rule
#
#   @return [String] the description of the rule
class Rule < ApplicationRecord
  belongs_to :user
  has_many :bottles, dependent: :nullify

  validates :name, :min_months, :max_months, presence: true
end
