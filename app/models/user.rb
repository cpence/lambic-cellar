# User encapsulates the information that we get from Google about a user via
# OpenID, plus the user's preferences.
#
# @!attribute uid
#   The user's ID (internal use only)
#
#   @api private
#   @raise [ValidationError] if `uid` is absent (`validates :presence`)
#   @raise [ValidationError] if `uid` is a duplicate (`validates :uniqueness`)
#   @return [String] the user ID (internal use only)
# @!attribute name
#   The user's full name
#
#   @raise [ValidationError] if `name` is absent (`validates :presence`)
#   @return [String] the user's full name
# @!attribute first_name
#   The user's first name
#
#   @raise [ValidationError] if `first_name` is absent (`validates :presence`)
#   @return [String] the user's first name
# @!attribute last_name
#   The user's last name
#
#   @raise [ValidationError] if `last_name` is absent (`validates :presence`)
#   @return [String] the user's last name
# @!attribute email
#   The user's email address
#
#   @raise [ValidationError] if `email` is absent (`validates :presence`)
#   @raise [ValidationError] if `email` is a duplicate (`validates :uniqueness`)
#   @return [String] the user's email address
# @!attribute image
#   A URL pointing to the user's profile picture
#
#   @raise [ValidationError] if `image` is absent (`validates :presence`)
#   @return [String] a URL pointing to the user's profile image
# @!attribute rules
#   A list of aging rules belonging to this user
#
#   @return [ActiveRecord::Relation] this user's aging rules
# @!attribute bottles
#   A list of bottles belonging to this user
#
#   @return [ActiveRecord::Relation] this user's bottles
class User < ApplicationRecord
  has_many :rules, dependent: :destroy
  has_many :bottles, through: :rules

  validates :uid, :name, :first_name, :last_name, :email, :image, presence: true
  validates :uid, :email, uniqueness: true

  # If true, the user was just added to the database
  #
  # @return [Boolean] if true, this user was added to the DB at their last login
  attr_accessor :new_user

  # Find or create a user based on the OpenID API response from Google
  #
  # @param google_response [Hash] the OpenID API response
  # @return [User] the user object, created and saved if new
  # @raise [RecordInvalid] if validations on the new user fail
  #
  # @example Parse a basic Google OpenID response.
  #   User.find_or_create_from_google({
  #     'provider' => 'google_oauth2',
  #     'uid' => '123456789',
  #     'info' => {
  #       'name' => 'John Smith',
  #       'email' => 'john@example.com',
  #       'first_name' => 'John',
  #       'last_name' => 'Smith',
  #       'image' => 'https://lh4.googleusercontent.com/photo.jpg',
  #       'urls' => {
  #         'google' => 'https://plus.google.com/+JohnSmith'
  #       }
  #     },
  #     'credentials' => {
  #       'token' => 'TOKEN',
  #       'refresh_token' => 'REFRESH_TOKEN',
  #       'expires_at' => 1_496_120_719,
  #       'expires' => true
  #     },
  #     'extra' => {
  #       'id_token' => 'ID_TOKEN',
  #       'id_info' => {
  #         'azp' => 'APP_ID',
  #         'aud' => 'APP_ID',
  #         'sub' => '100000000000000000000',
  #         'email' => 'john@example.com',
  #         'email_verified' => true,
  #         'at_hash' => 'HK6E_P6Dh8Y93mRNtsDB1Q',
  #         'iss' => 'accounts.google.com',
  #         'iat' => 1_496_117_119,
  #         'exp' => 1_496_120_719
  #       },
  #       'raw_info' => {
  #         'sub' => '100000000000000000000',
  #         'name' => 'John Smith',
  #         'given_name' => 'John',
  #         'family_name' => 'Smith',
  #         'profile' => 'https://plus.google.com/+JohnSmith',
  #         'picture' => 'https://lh4.googleusercontent.com/photo.jpg?sz=50',
  #         'email' => 'john@example.com',
  #         'email_verified' => 'true',
  #         'locale' => 'en',
  #         'hd' => 'company.com'
  #       }
  #     }
  #   }) #=> User(uid: '123456789', name: 'John Smith', ...)
  def self.find_or_create_from_google(google_response)
    # Check for an existing user
    uid = google_response["uid"]
    user = User.find_by(uid:) unless uid.nil?
    if user
      user.new_user = false
      return user
    end

    # Create a new user
    user_params = {
      uid:,
      name: google_response.dig("info", "name"),
      first_name: google_response.dig("info", "first_name"),
      last_name: google_response.dig("info", "last_name"),
      email: google_response.dig("info", "email"),
      image: google_response.dig("info", "image")
    }

    user = User.create!(user_params)
    user.new_user = true
    user
  end
end
