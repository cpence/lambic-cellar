# Lambic Cellar Privacy Policy

Lambic Cellar is run as a freely accessible, open-source product by [Charles Pence.](https://charlespence.net) This privacy policy will explain how we use the personal data we collect from you when you use our website.

## Topics:

- [What data do we collect?](#what-data-do-we-collect)
- [How do we collect your data?](#how-do-we-collect-your-data)
- [How do we store your data?](#how-do-we-store-your-data)
- [Marketing](#marketing)
- [What are your data protection rights?](#what-are-your-data-protection-rights)
- [What are cookies?](#what-are-cookies)
- [How do we use cookies?](#how-do-we-use-cookies)
- [What types of cookies do we use?](#what-types-of-cookies-do-we-use)
- [How to manage your cookies](#how-to-manage-your-cookies)
- [Privacy policies of other websites](#privacy-policies-of-other-websites)
- [Changes to our privacy policy](#changes-to-our-privacy-policy)
- [How to contact us](#how-to-contact-us)
- [How to contact the appropriate authorities](#how-to-contact-the-appropriate-authorities)

## What data do we collect?

We collect the following data:

- Personal identification information (name, email address, and profile image associated with your Google account)
- Any information about your beers that you explicitly enter into our system

Notably, we do not ever store your password, in any form. We ask Google to confirm your identity, but to do that we never have to see any of your login details.

## How do we collect your data?

You directly provide us with most of the data that we collect. We collect and process data from you when you:

- create a user account by signing in with Google
- edit the preferences for your user account
- fill in a form to create a new brewery, beer, or bottle
- use or view our website, via your browser's cookies

When you sign in to our website using Google, we receive from them:

- your name and email address
- your publicly available Google profile image, if set
- the user ID for your Google account (an internal Google value)

We have no access to any other information connected to your Google account, and we only use this information to authenticate your account (to be sure that it's really you).

## How will we use your data?

We collect your data so that we can:

- let you sign in to your user account (so that we can keep track of your beers)
- save the list of breweries, beers, and bottles that you've entered
- show you info about your beers

We will never, under any circumstances, for any reason, share any of this data with any other website or any other company.

## How do we store your data?

We securely store your data in data centers run by [Heroku.](http://heroku.com/) They do not have access to your data; for more information about their privacy and security policies, [see this page on their website.](https://www.heroku.com/policy/security)

We will keep your data for as long as you have an active user account on the website. If you would like to delete your account, you can choose the corresponding option in the user menu (found by clicking on your profile image on the top-right corner of the screen).

Once you delete your account, all traces of the information that you have entered will be removed, **except** for information about beers and breweries, which becomes part of our public database of beer information. Upon deletion of your account, all traces of your login information and bottle collection are removed.

## Marketing

We do not send you information about any other "products" that we have (we don't have any), and we will not use your contact information for marketing. Ever.

## What are your data protection rights?

We're located in the EU. That means that you have a variety of data protection rights, and we want to make sure that you know what they are. Every user here is entitled to the following:

- _The right to access:_ You have the right to request copies of your personal data. We don't have an automated way to do this yet, but if you want it, we'll export the information about your beer cellar so you can have it for safe-keeping. We're working on a way for you to make backups of this information as well.

- _The right to rectification:_ You have the right to request that we correct any information you believe is inaccurate. You also have the right to request that we complete the information you believe is incomplete. This shouldn't be a problem, though: information about your beers is editable, and information in your user profile comes straight from Google.

- _The right to erasure:_ You have the right to request that we erase your personal data. This is accomplished by deleting your account (in the user menu, located at the top-right corner of the screen).

- _The right to restrict processing:_ You have the right to request that we restrict the processing of your personal data.

- _The right to object to processing:_ You have the right to object to our processing of your personal data.

- _The right to data portability:_ You have the right to request that we transfer the data that we have collected to another organization, or directly to you, if we can.

If you make a request, we have one month to respond to you. If you would like to exercise any of these rights, please contact me by email: <charles@charlespence.net>

## What are cookies?

Cookies are text files placed on your computer to collect standard internet log information and visitor behavior information. When you visit our website, we will set a temporary cookie so that we can know when your browser is logged in or not.

For further information, visit <https://allaboutcookies.org>.

## How do we use cookies?

We use cookies to detect whether or not your browser has logged in. This is the only information that we store about you. Technically this kind of cookie is exempt from the GDPR (because it can't be used to track you), but we still thought you should know.

## What types of cookies do we use?

We set no persistent functionality, advertising, or behavioral tracking cookies, only so-called "session" cookies that are exempt from the GDPR.

## How to manage your cookies

You can set your browser not to accept cookies, and the above website tells you how to remove cookies from your browser. However, in a few cases, some of our website features may not function as a result. (You'll probably have to log in _lots_ of times.)

## Privacy policies of other websites

Lambic Cellar contains links to other websites. Our privacy policy applies only to our website, so if you click on a link to another website, you should read their privacy policy.

## Changes to our privacy policy

We keep our privacy policy under regular review and place any updates on this web page. This privacy policy was last updated on April 15, 2022.

## How to contact us

If you have any questions about our privacy policy, the data we hold on you, or you would like to exercise one of your data protection rights, please do not hesitate to contact us.

Email us at: <charles@charlespence.net>

## How to contact the appropriate authorities

Should you wish to report a complaint or if you feel that we have not addressed your concern in a satisfactory manner, you may contact the Data Protection Authority. As we are located in Belgium, this is:

Autorité de la protection des données – Gegevensbeschermingsautoriteit (APD-GBA)

Rue de la Presse 35 / Drukpersstraat 35  
1000 Bruxelles / 1000 Brussel

Tel. +32 2 274 48 00  
Fax +32 2 274 48 35  
<https://www.dataprotectionauthority.be/citizen>  
<commission@privacycommission.be>

_This privacy policy was written based upon the template available at <https://gdpr.eu>. We appreciate the help!_
