# Template handler for rendering Markdown views
module MarkdownHandler
  # Get a copy of Rails's ERB template handler
  #
  # @return [Object] the ERB template handler
  def self.erb
    @erb ||= ActionView::Template.registered_template_handler(:erb)
  end

  # Render a Markdown view template
  #
  # @param template [ActionView::Template] the template being rendered
  # @param source [String] the source of the loaded Markdown file
  # @return [String] Ruby code to render the source as Markdown
  def self.call(template, source)
    compiled_source = erb.call(template, source)
    <<~RET
      '<div class="container">' +
      Kramdown::Document.new(begin;#{compiled_source};end.to_s).to_html +
      '</div>'
    RET
  end
end

ActionView::Template.register_template_handler :md, MarkdownHandler
