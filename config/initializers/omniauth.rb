# Pass our Google credentials to OmniAuth
Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2,
           Rails.application.credentials.google_client_id,
           Rails.application.credentials.google_client_secret,
           { image_aspect_ratio: "square", image_size: 64 }
end

# This will route failed login attempts to a SessionsController action
OmniAuth.config.on_failure =
  proc { |env| OmniAuth::FailureEndpoint.new(env).redirect_to_failure }

# Log with the Rails logger
OmniAuth.config.logger = Rails.logger
