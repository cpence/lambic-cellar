# Enable the cookie store
Rails.application.config.session_store :cookie_store,
                                       key: "_lambic_cellar_session",
                                       httponly: true,
                                       secure: Rails.env.production?

Rails.application.config.middleware.use ActionDispatch::Cookies
Rails.application.config.middleware.use ActionDispatch::Session::CookieStore,
                Rails.application.config.session_options
