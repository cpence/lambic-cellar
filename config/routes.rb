Rails.application.routes.draw do
  # Beers
  resources :bottles do
    collection { get :drink }
  end

  # Other resources
  resources :beers, :breweries

  # This is a route handled by OmniAuth, give it a nice name
  direct :sign_in do
    "/auth/google_oauth2"
  end
  get "/auth/:provider/callback" => "sessions#create"

  # The rest of our routes for authentication
  get "/auth/failure" => "sessions#failure"
  post "/auth/sign_out" => "sessions#destroy", :as => :sign_out

  # User and settings management
  post "/auth/delete" => "sessions#delete", :as => :user_delete
  get "/auth/settings" => "sessions#show", :as => :user_settings

  # Static pages
  get "/about" => "static#about"
  get "/privacy" => "static#privacy"
  root "static#index"

  # Error pages
  get "/404" => "errors#not_found"
  get "/422" => "errors#unprocessable_entity"
  get "/500" => "errors#internal_server_error"
end
