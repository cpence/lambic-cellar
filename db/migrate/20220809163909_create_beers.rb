class CreateBeers < ActiveRecord::Migration[7.0]
  def change
    create_table :beers do |t|
      t.string :name
      t.references :brewery, null: false, foreign_key: true
      t.string :disambiguation
      t.text :description

      t.timestamps
    end
  end
end
