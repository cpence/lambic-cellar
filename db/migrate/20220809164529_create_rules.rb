class CreateRules < ActiveRecord::Migration[7.0]
  def change
    create_table :rules do |t|
      t.string :name
      t.references :user, null: false, foreign_key: true
      t.integer :min_months
      t.integer :max_months
      t.text :description

      t.timestamps
    end
  end
end
