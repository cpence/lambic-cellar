class CreateBottles < ActiveRecord::Migration[7.0]
  def change
    create_table :bottles do |t|
      t.string :name
      t.references :rule, null: false, foreign_key: true
      t.references :beer, null: false, foreign_key: true
      t.date :bottled_on
      t.text :description

      t.timestamps
    end
  end
end
