class AddDrunkToBottle < ActiveRecord::Migration[7.0]
  def change
    add_column :bottles, :drunk, :boolean, default: false
    add_column :bottles, :drunk_on, :date
  end
end
