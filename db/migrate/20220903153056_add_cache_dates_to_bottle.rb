class AddCacheDatesToBottle < ActiveRecord::Migration[7.0]
  def change
    add_column :bottles, :window_start, :date
    add_column :bottles, :window_end, :date
  end
end
