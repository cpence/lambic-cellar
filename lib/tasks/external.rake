unless Rails.env.production?
  require "bundler/audit/task"
  Bundler::Audit::Task.new

  namespace :quality do
    desc "Run bundle-audit security analyzer"
    task bundle_audit: %w[bundle:audit:update bundle:audit]
  end
end

namespace :quality do
  desc "Run rubocop code analyzer"
  task rubocop: :environment do
    system("bundle exec rubocop")
  end

  desc "Run rails_best_practices static analyzer"
  task rails_bp: :environment do
    system(
      "bundle exec rails_best_practices -f html --output quality/rails_bp.html ."
    )
  end

  desc "Run reek code smell detector"
  task reek: :environment do
    system("bundle exec reek .")
  end

  desc "Run brakeman security analyzer"
  task brakeman: :environment do
    system("bundle exec brakeman -o quality/brakeman.html")
  end
end
