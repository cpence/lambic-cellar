namespace :fly do
  # BUILD step:
  #  - changes to the filesystem made here DO get deployed
  #  - NO access to secrets, volumes, databases
  #  - Failures here prevent deployment
  task :build do
    # Ignore the absence of the master key when precompiling assets
    ENV["RAILS_IGNORE_MASTER_KEY"] = "1"
    Rake::Task["assets:precompile"].invoke
    ENV.delete("RAILS_IGNORE_MASTER_KEY")
  end

  # RELEASE step:
  #  - changes to the filesystem made here are DISCARDED
  #  - full access to secrets, databases
  #  - failures here prevent deployment
  task release: "db:migrate"

  # SERVER step:
  #  - changes to the filesystem made here are deployed
  #  - full access to secrets, databases
  #  - failures here result in VM being stated, shutdown, and rolled back
  #    to last successful deploy (if any).
  task :server do
    sh "bundle exec puma -C config/puma.rb"
  end
end
