unless Rails.env.production?
  require "yardstick/rake/measurement"

  namespace :quality do
    yardstick_opts = YAML.load_file(Rails.root.join("lib/tasks/yardstick.yaml"))

    Yardstick::Rake::Measurement.new(:yardstick, yardstick_opts) do |ys|
      ys.path = %w[app/**/*.rb config/**/*.rb lib/**/*.rb]
      ys.output = "quality/yardstick.txt"
      ys.verbose = true
    end
  end
end
