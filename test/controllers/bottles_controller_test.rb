require "test_helper"

class BottlesControllerTest < ActionDispatch::IntegrationTest
  test "should get dashboard" do
    get beers_dashboard_url
    assert_response :success
  end

  test "should get cellar" do
    get beers_cellar_url
    assert_response :success
  end
end
