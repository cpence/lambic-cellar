require "test_helper"

class SessionsControllerTest < ActionDispatch::IntegrationTest
  setup { OmniAuth.config.test_mode = true }
  teardown { OmniAuth.config.test_mode = false }

  test "should not allow GET on the first auth step" do
    # This is the first half of a CVE test: we should not allow a GET request on
    # the auth-start endpoint
    assert_raises(ActionController::RoutingError) { get "/auth/google_oauth2" }
  end

  test "should raise a CSRF failure without a CSRF token" do
    # This is a test for the other half of the CVE: we should require a CSRF
    # token on the POST for the provider
    @allow_forgery_protection = ActionController::Base.allow_forgery_protection
    ActionController::Base.allow_forgery_protection = true
    @omni_auth_on_failure = OmniAuth.config.on_failure
    OmniAuth.config.on_failure = proc { |env| raise env["omniauth.error.type"] }

    Rails.logger.silence do
      assert_raises("ActionController::InvalidAuthenticityToken") do
        post "/auth/google_oauth2"
      end
    end

    OmniAuth.config.on_failure = @omni_auth_on_failure
    ActionController::Base.allow_forgery_protection = @allow_forgery_protection
  end

  test "authentication failure page should work" do
    OmniAuth.config.test_mode = false

    get "/auth/google_oauth2/callback"
    assert_redirected_to %r{/auth/failure.*}

    follow_redirect!
    assert_response :unprocessable_entity

    OmniAuth.config.test_mode = true
  end

  test "sign in of existing user should work" do
    user = create(:user)
    user_count = User.count
    sign_in(user)

    assert_equal user_count, User.count
    assert_equal user.id, session["current_user_id"]
    assert_equal I18n.t("sessions.signed_in"), flash[:notice]
  end

  test "sign in of new user should work" do
    user_count = User.count
    user = build(:user)
    sign_in(user)

    user = User.find_by!(uid: user.uid)
    assert_equal (user_count + 1), User.count
    assert_equal user.id, session["current_user_id"]
    assert_equal I18n.t("sessions.new_user"), flash[:notice]
  end

  test "sign out should work" do
    user = create(:user)
    sign_in(user)
    assert_equal user.id, session["current_user_id"]

    sign_out
    assert_nil session["current_user_id"]
    assert_equal I18n.t("sessions.signed_out"), flash[:notice]
  end

  test "delete should work" do
    user_count = User.count
    user = build(:user)
    sign_in(user)

    user = User.find_by!(uid: user.uid)
    assert_equal (user_count + 1), User.count
    assert_equal user.id, session["current_user_id"]

    # Delete it
    post "/auth/delete"
    assert_redirected_to root_path

    assert_equal user_count, User.count
    assert_nil User.find_by(uid: "asdfqwer")
  end

  test "delete should fail if not logged in" do
    post "/auth/delete"
    assert_response :forbidden
  end

  test "show settings should work" do
    sign_in(create(:user))

    get "/auth/settings"
    assert_response :success
  end

  test "show settings should fail if not logged in" do
    get "/auth/settings"
    assert_response :forbidden
  end
end
