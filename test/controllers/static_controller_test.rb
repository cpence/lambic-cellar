require "test_helper"

class StaticControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get root_url
    assert_response :success
  end

  test "should get about" do
    get about_url
    assert_response :success
  end

  test "should get privacy" do
    get privacy_url
    assert_response :success
  end

  test "should route root_path to static#index" do
    assert_routing(
      { path: "/", method: :get },
      { controller: "static", action: "index" }
    )
  end

  test "should route about to static#about" do
    assert_routing(
      { path: "/about", method: :get },
      { controller: "static", action: "about" }
    )
  end

  test "should route privacy policy to static#privacy" do
    # Note that this test should be changed with care, this privacy policy link
    # is sent to Google for app verification
    assert_routing(
      { path: "/privacy", method: :get },
      { controller: "static", action: "privacy" }
    )
  end
end
