FactoryBot.define do
  factory :user do
    sequence(:first_name) { |n| "Person#{n}" }
    last_name { "Lastnamerson" }
    name { "#{first_name} #{last_name}" }
    sequence(:email) { |n| "person#{n}@email.com" }
    image { "http://www.something.com/#{email}.jpg" }
    sequence(:uid) { |n| "93042asdf839482039#{n}" }
  end

  factory :rule do
    sequence(:name) { |n| "Aging Rule #{n}" }
    user
    min_months { 6 }
    max_months { 24 }
    description { "A description of an aging rule" }
  end

  factory :brewery do
    sequence(:name) { |n| "Brewery #{n}" }
  end

  factory :beer do
    sequence(:name) { |n| "Beer #{n}" }
    brewery
    disambiguation { "not like the other beers" }
    description { "the king of beers" }
  end

  factory :bottle do
    sequence(:name) { |n| "Bottle #{n}" }
    beer
    rule
    bottled_on { Date.new(2010, 4, 25) }
    drunk { false }
    description { "a special bottle" }
  end
end
