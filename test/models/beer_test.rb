require "test_helper"

class BeerTest < ActiveSupport::TestCase
  should validate_presence_of :name

  teardown { travel_back }

  def update_windows_with(bottle_hashes, rule_info = {})
    travel_to Date.new(2000, 1, 1)

    user = create(:user)
    beer = create(:beer)
    rule = create(:rule, **rule_info)

    bottles =
      bottle_hashes.map { |bh| create(:bottle, user:, beer:, rule:, **bh) }

    beer.update_windows(user)

    bottles.each(&:reload)
    bottles.sort_by { |b| [b.window_start, b.bottled_on] }
  end

  def assert_bottles(bottles, *dates)
    bottles.each_with_index do |b, i|
      assert_equal dates[i], b.window_start
      assert_equal (dates[i + 1] - 1.day), b.window_end
    end
  end

  test "update_windows works with one bottle" do
    bottles = update_windows_with([{ bottled_on: Date.new(1999, 4, 1) }])
    assert_bottles(bottles, Date.new(2000, 1, 1), Date.new(2001, 4, 1))
  end

  test "update_windows ignores drunk bottles" do
    bottles =
      update_windows_with(
        [
          {
            bottled_on: Date.new(2000, 1, 1),
            window_start: Date.new(1980, 1, 1),
            window_end: Date.new(1981, 1, 1),
            drunk: true
          }
        ]
      )

    assert_equal Date.new(1980, 1, 1), bottles[0].window_start
    assert_equal Date.new(1981, 1, 1), bottles[0].window_end
  end

  test "update_windows works with simultaneous bottles" do
    bottles =
      update_windows_with(
        [
          { bottled_on: Date.new(2000, 1, 1) },
          { bottled_on: Date.new(2000, 1, 1) },
          { bottled_on: Date.new(2000, 1, 1) }
        ]
      )

    assert_bottles(
      bottles,
      Date.new(2000, 7, 1),
      Date.new(2000, 12, 31),
      Date.new(2001, 7, 2),
      Date.new(2002, 1, 1)
    )
  end

  test "update_windows works with a complex case" do
    bottles =
      update_windows_with(
        [
          { bottled_on: Date.new(1999, 7, 1) },
          { bottled_on: Date.new(2000, 4, 1) },
          { bottled_on: Date.new(2001, 2, 1) },
          { bottled_on: Date.new(2001, 2, 1) },
          { bottled_on: Date.new(2001, 2, 1) },
          { bottled_on: Date.new(2001, 2, 1) },
          { bottled_on: Date.new(2001, 10, 1) }
        ],
        { min_months: 6, max_months: 18 }
      )

    assert_bottles(
      bottles,
      Date.new(2000, 1, 1),
      Date.new(2000, 11, 16),
      Date.new(2001, 8, 13),
      Date.new(2001, 11, 15),
      Date.new(2001, 12, 31),
      Date.new(2002, 2, 14),
      Date.new(2002, 6, 1),
      Date.new(2003, 4, 1)
    )
  end
end
