require "test_helper"

class BottleTest < ActiveSupport::TestCase
  should validate_presence_of :bottled_on

  test "window_earliest works with past" do
    user = create(:user)
    rule = create(:rule, user:, min_months: 5)
    bottle = create(:bottle, rule:, bottled_on: Date.new(2000, 1, 1))

    assert_equal(Date.new(2000, 6, 1), bottle.window_earliest(past: true))
  end

  test "window_earliest works without past" do
    travel_to Date.new(2003, 1, 1)

    user = create(:user)
    rule = create(:rule, user:, min_months: 5)
    bottle = create(:bottle, rule:, bottled_on: Date.new(2000, 1, 1))

    assert_equal(Date.new(2003, 1, 1), bottle.window_earliest)

    travel_back
  end

  test "window_latest works" do
    rule = create(:rule, max_months: 12)
    bottle = create(:bottle, rule:, bottled_on: Date.new(2000, 1, 1))

    assert_equal(Date.new(2001, 1, 1), bottle.window_latest)
  end
end
