require "test_helper"

class BreweryTest < ActiveSupport::TestCase
  should validate_presence_of :name
end
