require "test_helper"

class RuleTest < ActiveSupport::TestCase
  should validate_presence_of :name
  should validate_presence_of :min_months
  should validate_presence_of :max_months
end
