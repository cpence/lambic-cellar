require "test_helper"

class UserTest < ActiveSupport::TestCase
  should validate_presence_of :uid
  should validate_presence_of :name
  should validate_presence_of :first_name
  should validate_presence_of :last_name
  should validate_presence_of :email
  should validate_presence_of :image

  should validate_uniqueness_of :uid
  should validate_uniqueness_of :email

  # Sample Google OAuth2 response, taken straight from their docs
  OAUTH2_RESPONSE = {
    "provider" => "google_oauth2",
    "uid" => "123456789",
    "info" => {
      "name" => "John Smith",
      "email" => "john@example.com",
      "first_name" => "John",
      "last_name" => "Smith",
      "image" => "https://lh4.googleusercontent.com/photo.jpg",
      "urls" => {
        "google" => "https://plus.google.com/+JohnSmith"
      }
    },
    "credentials" => {
      "token" => "TOKEN",
      "refresh_token" => "REFRESH_TOKEN",
      "expires_at" => 1_496_120_719,
      "expires" => true
    },
    "extra" => {
      "id_token" => "ID_TOKEN",
      "id_info" => {
        "azp" => "APP_ID",
        "aud" => "APP_ID",
        "sub" => "100000000000000000000",
        "email" => "john@example.com",
        "email_verified" => true,
        "at_hash" => "HK6E_P6Dh8Y93mRNtsDB1Q",
        "iss" => "accounts.google.com",
        "iat" => 1_496_117_119,
        "exp" => 1_496_120_719
      },
      "raw_info" => {
        "sub" => "100000000000000000000",
        "name" => "John Smith",
        "given_name" => "John",
        "family_name" => "Smith",
        "profile" => "https://plus.google.com/+JohnSmith",
        "picture" => "https://lh4.googleusercontent.com/photo.jpg?sz=50",
        "email" => "john@example.com",
        "email_verified" => "true",
        "locale" => "en",
        "hd" => "company.com"
      }
    }
  }.freeze

  test "should create a new user from Google callback" do
    user_count = User.count

    user = User.find_or_create_from_google(OAUTH2_RESPONSE.dup)

    assert_equal (user_count + 1), User.count
    assert_instance_of User, user
    assert_equal User.find_by(uid: "123456789"), user
  end

  test "should find existing user from Google callback" do
    User.find_or_create_from_google(OAUTH2_RESPONSE.dup)
    user_count = User.count

    user = User.find_or_create_from_google(OAUTH2_RESPONSE.dup)

    assert_not_nil user
    assert_equal user_count, User.count
    assert_instance_of User, user
  end
end
