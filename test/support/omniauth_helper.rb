module OmniAuthHelper
  GOOGLE_HASH = {
    provider: "google_oauth2",
    credentials: {
      token: "token",
      refresh_token: "another_token",
      expires_at: 1_354_920_555,
      expires: true
    },
    extra: {
      raw_info: {
        sub: "123456789",
        email: "raw@email.com",
        email_verified: true,
        name: "Raw Name",
        given_name: "Raw",
        family_name: "Name",
        profile: "https://plus.google.com/123456789",
        picture: "http://raw.image",
        gender: "male",
        birthday: "0000-06-25",
        locale: "en",
        hd: "company_name.com"
      }
    }
  }.freeze

  def google_hash_for(user)
    GOOGLE_HASH.dup.merge(
      {
        uid: user.uid,
        info: {
          name: user.name,
          email: user.email,
          first_name: user.first_name,
          last_name: user.last_name,
          image: user.image
        }
      }
    )
  end

  def sign_in(user)
    OmniAuth.config.mock_auth[:google_oauth2] = OmniAuth::AuthHash.new(
      google_hash_for(user)
    )

    # We post to the provider URL and get redirected to the callback
    post "/auth/google_oauth2"
    assert_redirected_to "/auth/google_oauth2/callback"
    follow_redirect!

    # The callback will then redirect us to the root
    assert_redirected_to root_path
    follow_redirect!
  end

  def sign_out
    # Post to sign out, which redirects to root
    post "/auth/sign_out"
    assert_redirected_to root_path
    follow_redirect!
  end
end
