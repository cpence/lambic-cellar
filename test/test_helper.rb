ENV["RAILS_ENV"] ||= "test"
require_relative "../config/environment"
require "rails/test_help"

# Load our test helpers
Dir[Rails.root.join("test/support/*")].each { |helper| require helper }

module ActiveSupport
  class TestCase
    # Run tests in parallel with specified workers
    parallelize(workers: :number_of_processors)

    # Add our loaded test helpers
    include FactoryBot::Syntax::Methods
    include OmniAuthHelper
  end
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :minitest
    with.library :rails
  end
end
